import scala.io.Source

object Ski extends App {
  type length = Int
  type drop = Int
  type pos = (Int, Int)
  type next = pos
  type result = (length, drop, next)

  val DELTA = List((-1, 0), (0, -1), (1, 0), (0, 1))

  def checkBound(r: Int, c: Int): Boolean = (r >= 0) && (r < row) && (c >= 0) && (c < col)

  def dfs(r: Int, c: Int): Unit = {
    val lowerNeighbors = for {
      (dr, dc) <- DELTA
      rr = r + dr; cc = c + dc
      if checkBound(rr, cc) && skiMap(rr)(cc) < skiMap(r)(c)
    } yield (rr, cc)

    var result: result = (1, 0, null)

    for ((rr, cc) <- lowerNeighbors) {
      if (!visited(rr, cc)) {
        dfs(rr, cc)
      }
      val (tLength, tDrop, _) = skiMapResult(rr)(cc)
      if (tLength + 1 > result._1 || (tLength + 1 == result._1 && skiMap(r)(c) - skiMap(rr)(cc) + tDrop > result._2))
        result = (tLength + 1, skiMap(r)(c) - skiMap(rr)(cc) + tDrop, (rr, cc))
    }
    skiMapResult(r)(c) = result
  }

  def start(): pos = {
    var resultPos = (0, 0)
    var result = skiMapResult(0)(0)
    for (i <- 0 until row)
      for (j <- 0 until col)
        if (result._1 < skiMapResult(i)(j)._1 || (result._1 == skiMapResult(i)(j)._1) && result._2 < skiMapResult(i)(j)._2) {
          resultPos = (i, j)
          result = skiMapResult(i)(j)
        }
    resultPos
  }

  def path(row: Int, col: Int): String = {
    def nextPath(row: Int, col: Int): String = {
      val nextPos = skiMapResult(row)(col)._3
      if (nextPos != null) "-" + skiMap(row)(col) + nextPath(nextPos._1, nextPos._2)
      else "-" + skiMap(row)(col)
    }

    val nextPos = skiMapResult(row)(col)._3
    if (nextPos != null) skiMap(row)(col) + nextPath(nextPos._1, nextPos._2)
    else skiMap(row)(col).toString
  }

  def visited(i: Int, j: Int): Boolean = skiMapResult(i)(j) != null

  val lines: Iterator[String] = Source.fromResource("map.txt").getLines()
  val l = lines.next().trim().split(" ").map(_.toInt)
  val (row, col) = (l(0), l(1))
  val skiMap = lines.take(row).map(_.trim().split(" ").map(_.toInt)).toArray
  val skiMapResult = new Array[Array[result]](row)
  for (i <- 0 until row) skiMapResult(i) = new Array[result](col)

  for (i <- 0 until row)
    for (j <- 0 until col)
      if (!visited(i, j)) dfs(i, j)

  val (rStart, cStart) = start()
  println(s"Start: ($rStart, $cStart)")
  println("Path: " + path(rStart, cStart))
  val result = skiMapResult(rStart)(cStart)
  val (length, drop) = (result._1, result._2)
  println(s"Length: $length, Drop: $drop")

}
